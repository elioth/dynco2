# DynCO2
A python package developed by Elioth (https://elioth.com/) to compute the instantaneous global warming impact (radiative forcing) of time series of emissions of greenhouse gases, based on the DynCO2 model by CIRAIG

# Credits / Sources
Annie Levasseur, CIRAIG
http://ciraig.org/index.php/fr/project/dynco2-calculateur-dempreinte-carbone-dynamique/

# Installation
Download the package archive at : https://gitdoit.egis.fr/elioth/dynco2/blob/master/dist/dynco2-0.1.tar.gz
Install the package with pip in your conda environment :
```
pip install path/to/dynco2-0.1.tar.gz
```

# Build
```
python setup.py sdist
```
# Licence
DynCO2 is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License
GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007
See the file LICENSE

# References
Forster, P. et al. (2007). Changes in Atmospheric Constituents and in Radiative Forcing. In S. Solomon et al. (Ed.), Climate Change 2007: The Physical Science Basic. Contribution of Working Group I to the Fourth Assessment Report of the Intergovernmental Panel on Climate Change (pp. 129-234). Cambridge, United Kingdom and New-York, NY, USA: Cambridge University Press.

Levasseur, A., Lesage, P., Margni, M., Deschênes, L., & Samson, R. (2010). Considering time in LCA: dynamic LCA and its application to global warming impact assessments. Environmental Science & Technology, 44(8), 3169-3174.
