#DynCO2 is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import dynco2
import numpy as np

# Initialize the DynCO2 model
dynco2_model = dynco2.DynCO2()

# Choose a time horizon for the cumulative impact assessment (= GWP)
time_horizon = 100

# Emission scenario 1 :
# one pulse of emissions at the beginning of the lifecycle of a building
emissions = np.zeros(2000)
emissions[0] = 1000
gwi = dynco2_model.compute_GWI(emissions, "CO2")
rf1 = gwi[0:(time_horizon-1)].sum()

# Emission scenario 2 :
# two pulses 25 years apart at the beginning and half of the lifecycle of a building
emissions = np.zeros(2000)
emissions[0] = 500
emissions[24] = 500
gwi = dynco2_model.compute_GWI(emissions, "CO2")
rf2 = gwi[0:(time_horizon-1)].sum()

# Ratio of the two scenarios
ratio = rf2/rf1
print(ratio)
