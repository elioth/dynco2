#DynCO2 is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import dynco2
import numpy as np

# Initialize the DynCO2 model
dynco2_model = dynco2.DynCO2()

# Create an emission profile for 100 years, covering the lifecycle of a building :
# - 1st phase : carbon sequestration during 40 years = tree growth
# - 2nd phase : zero emissions during 50 years, the lifetime of the building
# - 3rd phase : complete release of the carbon during demolition and disposal of the buildings materials
emissions = np.zeros(2000)
emissions[0:40] = -1000/40
emissions[89] = 1000

# Compute the instantaneous global warming impact, in W/m²
gwi = dynco2_model.compute_GWI(emissions, "CO2")


emissions = np.zeros(2000)
emissions[0] = 1000
gwi = dynco2_model.compute_GWI(emissions, "CO2")
rf1 = gwi[0:99].sum()

emissions = np.zeros(2000)
emissions[0] = 500
emissions[24] = 500
gwi = dynco2_model.compute_GWI(emissions, "CO2")
rf2 = gwi[0:99].sum()

rf2/rf1
