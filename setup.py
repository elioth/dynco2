from distutils.core import setup

setup(
      name='dynco2',
      version='0.1',
      description='Python Distribution Utilities',
      author='Félix Pouchain',
      author_email='f.pouchain@elioth.fr',
      packages=['dynco2'],
      package_dir={'dynco2': 'src/dynco2'},
      package_data={'dynco2': ["data/dcf.csv"]}
)