#DynCO2 is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import pkg_resources
import pandas as pd
import numpy as np

class DynCO2:
    
    def __init__(self):
        dcf_data_path = pkg_resources.resource_filename('dynco2', "data/dcf.csv")
        self.dcf_db = pd.read_csv(dcf_data_path)
        
    def get_available_gases_list(self):
        return [col for col in self.dcf_db.columns if col != "year"]
        
    def compute_GWI(self, emissions, gas):
        
        if gas not in self.dcf_db.columns:
            raise ValueError("Gas '" + gas + "' is not available in the DynCO2 model. Check the available gases list with the get_available_gases_list function.")
        
        dcf = self.dcf_db[gas]
        
        gwi = np.convolve(emissions, dcf)
        gwi = gwi[0:len(emissions)]
        
        return gwi
