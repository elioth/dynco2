#DynCO2 is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import pandas as pd

# Load DYNCO2 dynamic characterization factors for each GHG
dcf = pd.read_excel(
    "../data/Dynamic_LCAcalculatorv.2.0.xlsm",
    header=3,   
    sheet_name="FC"
)

dcf = dcf.drop(dcf.index[[0, 1]])
dcf.columns = ["year"] + list(dcf.columns[1:len(dcf.columns)])

# Format data
dcf = dcf.melt(id_vars="year")
dcf.columns = ["year", "gas", "value"]
dcf["value"] = dcf["value"].astype(float)
dcf = pd.pivot(dcf, index="year", columns="gas", values="value")
dcf.reset_index(inplace=True)

# Save data as csv
dcf.to_csv("../data/dcf.csv", index=False)
